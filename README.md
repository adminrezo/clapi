# Clapi

An API for ClamAV

## Description

Need to check if a file with AV before uploading it ?

Ask to Clapi !

Clapi is a FOSS Antivirus API based on ClamAV, the open source Anti Virus
supported by Cisco Talos with lots of extra signatures sources. 

![ClamAV Free Logo](public/ClamAVLogo_med.png)

## Badges

![coverage](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage)

## Installation

You'll need to install :

- Docker
- Docker Compose

Clone the repository and the submodules :

    git clone https://gitlab.com/adminrezo/clapi.git
    cd clapi
    git submodule init && git submodule update

Optionnal : You can build Cisco's ClamAV image :

    docker build -t clamav/clamav:stable_base Images/ClamAV

Then, build with Compose

    docker-compose build


## Usage

To run the stuff :

    docker-compose up -d

You'll have those services listening :

- Public :
  - CLAPI, the REST API on **port 8080**
- Private
  - Clamdscan on **port 13310**
  - (optionnal) CVD update on **port 18000**

Test with curl :

    # A malicious file (Eicar test)
    curl -s -XPOST http://localhost:8080/api/v1/scan -F FILES=@Test/eicar_com.zip
    # A sane file
    curl -s -XPOST http://localhost:8080/api/v1/scan -F FILES=@Test/clean_file.txt
    # Many files at the same time
    curl -s -XPOST http://localhost:8080/api/v1/scan -F FILES=@file1 -F FILES=@file2 -F FILES=@fileN


## Support

Please use Gitlab's issues

## Roadmap

TODO

## Contributing

1. Fork the repo
2. Create a new feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request

## Authors and acknowledgment

This project is developed and maintained by Nicolas Dewaele.

## License

This project is under the GPLv3 free license.

The components used are :

- [ClamAV](https://talosintelligence.com/clamav) by **Cisco Talos** under [GPLv2 License](https://github.com/Cisco-Talos/clamav#licensing)
- [CVD Update](https://pypi.org/project/cvdupdate/) by **Cisco Talos** under [Apache v2 License](https://github.com/Cisco-Talos/cvdupdate#license)
- [Yara](https://github.com/VirusTotal/yara) by **VirusTotal** under [BSDv3 License](https://github.com/VirusTotal/yara/blob/master/COPYING)
- [Roda](https://github.com/jeremyevans/roda) for the API
- [ClamAV client (Ruby)](https://github.com/franckverrot/clamav-client/)
- [ClamAV Rest (Ruby)](https://hub.docker.com/r/lokori/clamav-rest/)
- [ClamAV Rest API (Ruby)](https://github.com/benzino77/clamav-rest-api)
- [Yara](https://docs.clamav.net/manual/Signatures/YaraRules.html#using-yara-rules-in-clamav)
- [ClamAV Unofficial Sigs](https://github.com/extremeshok/clamav-unofficial-sigs)



## Project status
TODO
